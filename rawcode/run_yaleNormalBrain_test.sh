#!/bin/bash
#MSUB -l nodes=10:ppn=4
#MSUB -A b1017 
#MSUB -q buyin 
#MSUB -N yaleNormBrain
#MSUB -m abe 
#MSUB -l walltime=00:10:00

cd $PBS_O_WORKDIR
module load R
#ulimit -t 12000000 -v 12000000
R CMD BATCH yaleNormalBrain_test.R


